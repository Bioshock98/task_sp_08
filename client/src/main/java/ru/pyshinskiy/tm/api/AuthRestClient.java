package ru.pyshinskiy.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.UserDTO;

import java.util.HashSet;

@Component
public class AuthRestClient extends AbstractRestClient {

    public void login(@NotNull final String username, @NotNull final String password) {
        @NotNull final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("username", username);
        httpHeaders.add("password", password);
        @NotNull final HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        restTemplate.postForEntity(loginUrl, httpEntity, RestAuthResult.class);
    }

    public void sign_up(@NotNull final String username, @NotNull final String password) {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(username);
        userDTO.setPasswordHash(password);
        userDTO.setRoles(new HashSet<>());
        restTemplate.postForObject(signUpUrl, userDTO, UserDTO.class);
    }

    public void acc_delete() {
        restTemplate.delete(accDeleteUrl);
    }
}
