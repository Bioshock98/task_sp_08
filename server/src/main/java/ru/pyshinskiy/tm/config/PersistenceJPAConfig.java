package ru.pyshinskiy.tm.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:database.properties")
@ComponentScan(basePackages = "ru.pyshinskiy.tm")
@EnableJpaRepositories(basePackages = "ru.pyshinskiy.tm.repository")
public class PersistenceJPAConfig {

    @Bean
    public DataSource dataSource(
            @Value("${db.driver}") final String dataSourceDriver,
            @Value("${db.url}") final String dataSourceUrl,
            @Value("${db.user}") final String dataSourceUser,
            @Value("${db.password}") final String dataSourcePassword
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            final DataSource dataSource,
            @Value("${hibernate.show_sql}") final boolean showSql,
            @Value("${hibernate.hbm2ddl.auto}") final String tableStrategy,
            @Value("${hibernate.dialect}") final String dialect,
            @Value("${hibernate.cache.use_second_level_cache}") final boolean userSecondLevelCache,
            @Value("${hibernate.cache.use_query_cache}") final boolean userQueryCache,
            @Value("${hibernate.cache.use_minimal_puts}") final boolean userMinimalPuts,
            @Value("${hibernate.cache.hazelcast.use_lite_member}") final boolean userLiteMember,
            @Value("${hibernate.cache.region_prefix}") final String regionPrefix,
            @Value("${hibernate.cache.provider_configuration_file_resource_path}") final String hazelcastConfigFilePath,
            @Value("${hibernate.cache.region.factory_class}") final String factoryClass
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.pyshinskiy.tm.model");
        final Properties properties = new Properties();
        properties.put("hibernate.show_sql", showSql);
        properties.put("hibernate.hbm2ddl.auto", tableStrategy);
        properties.put("hibernate.dialect", dialect);
        properties.put("hibernate.cache.use_second_level_cache", userSecondLevelCache);
        properties.put("hibernate.cache.use_query_cache", userQueryCache);
        properties.put("hibernate.cache.use_minimal_puts", userMinimalPuts);
        properties.put("hibernate.cache.hazelcast.use_lite_member", userLiteMember);
        properties.put("hibernate.cache.region_prefix", regionPrefix);
        properties.put("hibernate.cache.provider_configuration_file_resource_path", hazelcastConfigFilePath);
        properties.put("hibernate.cache.region.factory_class", factoryClass);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(final LocalContainerEntityManagerFactoryBean emf) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf.getObject());
        return transactionManager;
    }
}
