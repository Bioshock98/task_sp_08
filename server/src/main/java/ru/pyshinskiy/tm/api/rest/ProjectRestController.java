package ru.pyshinskiy.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/project")
public class ProjectRestController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @GetMapping("/all")
    public ResponseEntity<List<ProjectDTO>> getAll(@AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        return ResponseEntity.ok(projectService.findAllProjectsByUserId(userPrincipal.getId())
                .stream()
                .map(e -> dtoConverter.toProjectDTO(e))
                .collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    public ResponseEntity<ProjectDTO> getProject(@PathVariable("id") @NotNull final String id,
                                                 @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        return ResponseEntity.ok(dtoConverter.toProjectDTO(projectService.findProjectByUserId(userPrincipal.getId(), id)));
    }

    @PostMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ProjectDTO> saveProject(@RequestBody @NotNull final ProjectDTO projectDTO,
                                                  @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        projectDTO.setUserId(userPrincipal.getId());
        projectService.save(dtoConverter.toProject(projectDTO));
        return ResponseEntity.ok(projectDTO);
    }

    @PutMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public void updateProject(@RequestBody @NotNull final ProjectDTO projectDTO,
                              @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        projectDTO.setUserId(userPrincipal.getId());
        projectService.save(dtoConverter.toProject(projectDTO));
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteProject(@PathVariable("id") @NotNull final String id,
                              @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        projectService.removeByUserId(id, userPrincipal.getId());
    }
}
