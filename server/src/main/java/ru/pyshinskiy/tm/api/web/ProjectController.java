package ru.pyshinskiy.tm.api.web;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.enumerated.Status;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Nullable
    private ProjectDTO selectedProject;

    @Nullable
    private ProjectDTO editingProject;

    @Nullable
    private ProjectDTO addingProject;

    @Nullable
    private ProjectDTO removingProject;

    @Nullable
    private List<ProjectDTO> filteredProjects;

    @NotNull
    private List<String> statuses = Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList());

    @NotNull
    private String projectAction = "Project add";

    @NotNull
    public ProjectDTO getAddingProject() {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        projectDTO.setUserId(userPrincipal.getId());
        return projectDTO;
    }

    public List<ProjectDTO> getProjects() {
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return projectService.findAllProjectsByUserId(userPrincipal.getId()).stream().map(e -> dtoConverter.toProjectDTO(e)).collect(Collectors.toList());
    }

    public String updateProject() {
        projectService.save(dtoConverter.toProject(editingProject));
        return "projects.xhtml?faces-redirect=true";
    }

    public String deleteProject() {
        projectService.remove(removingProject.getId());
        return "projects.xhtml?faces-redirect=true";
    }
}
