package ru.pyshinskiy.tm.dto;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.pyshinskiy.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public class ProjectDTO extends AbstractDTO {

    @Nullable
    private String userId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Date createTime = new Date(System.currentTimeMillis());

    @Nullable
    private Status status = Status.PLANNED;

    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date startDate;

    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date finishDate;
}
