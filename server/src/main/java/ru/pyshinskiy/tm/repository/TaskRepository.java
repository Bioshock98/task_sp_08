package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    @Nullable
    Task findTaskById(@NotNull final String id);

    @NotNull
    List<Task> findTasksByProject(@NotNull final Project project);

    @NotNull
    List<Task> findTasksByUserId(@NotNull final String userId);

    @Nullable
    Task findTaskByIdAndUserId(@NotNull final String id, @NotNull final String userId);

    void deleteTaskByIdAndUserId(@NotNull final String taskId, @NotNull final String userId);
}
