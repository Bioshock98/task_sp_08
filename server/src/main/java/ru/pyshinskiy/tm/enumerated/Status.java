package ru.pyshinskiy.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("Planed"),
    IN_PROGRESS("In progress"),
    DONE("Done");

    @NotNull
    private String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
